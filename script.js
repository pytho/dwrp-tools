// ==UserScript==
// @name         DWRP Tools
// @namespace    https://pytho.carrd.co/
// @version      0.1.5
// @description  Adds tools to make tagging easier
// @author       Pytho
// @match        *://*.dreamwidth.org/*
// @grant        GM_addElement
// ==/UserScript==
(function() {
    'use strict';

  GM_addElement('link', {
      rel: 'preconnect',
      href: 'https://fonts.googleapis.com'
  });

  GM_addElement('link', {
      rel: 'preconnect',
      href: 'https://fonts.gstatic.com',
      crossorigin: true
  });

  GM_addElement('link', {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap'
  });

  GM_addElement('style', {
      type: 'text/css',
      textContent: 'body { font-family:"Noto Sans", sans-serif; }'
  });
  
  const placeholder = 'TEXT';
  const actionAStart = '<small>[ ';
  const actionAEnd = ' ]</small>';
  const actionBStart = '<small>( ';
  const actionBEnd = ' )</small>';
  const txtStart = '<code>';
  const txtEnd = '</code>';

  const btnActionA = document.createElement('button');
  btnActionA.appendChild(document.createTextNode('[ ]'));
  btnActionA.onclick = addActionACode;

  const btnActionB = document.createElement('button');
  btnActionB.appendChild(document.createTextNode('( )'));
  btnActionB.onclick = addActionBCode;

  const btnTexting = document.createElement('button');
  btnTexting.appendChild(document.createTextNode('code'));
  btnTexting.onclick = addTextingCode;

  const btnItalics = document.createElement('button');
  btnItalics.appendChild(document.createTextNode('<i>'));
  btnItalics.onclick = addItalics;

  const btnBold = document.createElement('button');
  btnBold.appendChild(document.createTextNode('<b>'));
  btnBold.onclick = addBold;

  const subject = document.getElementById('subject') || document.getElementsByName('subject')[0];
  const body = document.getElementById('body') || document.getElementById('commenttext') || document.getElementsByName('body')[0];

  if (subject && subject.parentNode) {
    subject.parentNode.appendChild(btnActionA);
    subject.parentNode.appendChild(btnActionB);
    subject.parentNode.appendChild(btnTexting);
    subject.parentNode.appendChild(btnItalics);
    subject.parentNode.appendChild(btnBold);
  }

  function addTextingCode(e) {
    e.preventDefault();
    if (body) {
      const head = body.value.slice(0, body.selectionStart);
      const selection = body.value.slice(body.selectionStart, body.selectionEnd) || placeholder;
      const tail = body.value.slice(body.selectionEnd);
      body.value = head + txtStart + selection + txtEnd + tail;
      body.focus();
      body.setSelectionRange((head + txtStart).length, (head + txtStart + selection).length);
    }
  }

  function addActionACode(e) {
    e.preventDefault();
    if (body) {
      const head = body.value.slice(0, body.selectionStart);
      const selection = body.value.slice(body.selectionStart, body.selectionEnd) || placeholder;
      const tail = body.value.slice(body.selectionEnd);
      body.value = head + actionAStart + selection + actionAEnd + tail;
      body.focus();
      body.setSelectionRange((head + actionAStart).length, (head + actionAStart + selection).length);
    }
  }

  function addActionBCode(e) {
    e.preventDefault();
    if (body) {
      const head = body.value.slice(0, body.selectionStart);
      const selection = body.value.slice(body.selectionStart, body.selectionEnd) || placeholder;
      const tail = body.value.slice(body.selectionEnd);
      body.value = head + actionBStart + selection + actionBEnd + tail;
      body.focus();
      body.setSelectionRange((head + actionBStart).length, (head + actionBStart + selection).length);
    }
  }

  function addItalics(e) {
    e.preventDefault();
    if (body) {
      const iStart = '<i>';
      const iEnd = '</i>';
      const head = body.value.slice(0, body.selectionStart);
      const selection = body.value.slice(body.selectionStart, body.selectionEnd) || placeholder;
      const tail = body.value.slice(body.selectionEnd);
      body.value = head + iStart + selection + iEnd + tail;
      body.focus();
      body.setSelectionRange((head + iStart).length, (head + iStart + selection).length);
    }
  }

  function addBold(e) {
    e.preventDefault();
    if (body) {
      const bStart = '<b>';
      const bEnd = '</b>';
      const head = body.value.slice(0, body.selectionStart);
      const selection = body.value.slice(body.selectionStart, body.selectionEnd) || placeholder;
      const tail = body.value.slice(body.selectionEnd);
      body.value = head + bStart + selection + bEnd + tail;
      body.focus();
      body.setSelectionRange((head + bStart).length, (head + bStart + selection).length);
    }
  }
})();
